cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
      "file": "plugins/cordova-plugin-console/www/console-via-logger.js",
      "id": "cordova-plugin-console.console",
      "clobbers": [
          "console"
      ]
  },
  {
      "file": "plugins/cordova-plugin-console/www/logger.js",
      "id": "cordova-plugin-console.logger",
      "clobbers": [
          "cordova.logger"
      ]
  },
  {
      "file": "plugins/cordova-plugin-device/www/device.js",
      "id": "cordova-plugin-device.device",
      "clobbers": [
          "device"
      ]
  },
  {
      "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
      "id": "cordova-plugin-inappbrowser.inappbrowser",
      "clobbers": [
          "cordova.InAppBrowser.open",
          "window.open"
      ]
  },
  {
      "file": "plugins/cordova-plugin-device-motion/www/Acceleration.js",
      "id": "cordova-plugin-device-motion.Acceleration",
      "clobbers": [
          "Acceleration"
      ]
  },
  {
      "file": "plugins/cordova-plugin-device-motion/www/accelerometer.js",
      "id": "cordova-plugin-device-motion.accelerometer",
      "clobbers": [
          "navigator.accelerometer"
      ]
  },

];
module.exports.metadata =
// TOP OF METADATA
{
  "cordova-plugin-console": "1.0.1-dev",
  "cordova-plugin-device": "1.0.1-dev",
  "cordova-plugin-inappbrowser": "1.0.0",
  "cordova-plugin-device-motion": "1.1.1-dev",


}
// BOTTOM OF METADATA
});
