/**
 * 
 */

 
settings = {
	serviceUrl:function() {
		return proxy.url( this._serviceUrl ); 
	},
	isAndroid: function() {
		return navigator.userAgent.match(/Android/i);
	},
	isBlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	isIOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	isOpera: function() {
    return navigator.userAgent.match(/Opera Mini/i);
	},
	isWindows: function() {
		return navigator.userAgent.match(/IEMobile/i);
	}

}; 
