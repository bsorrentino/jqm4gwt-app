/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var proxy = {
    
    address:'localhost:8080',
    enabled: false,
    
    url:function( url ) {
        if( this.enabled ) {
        
            var regex = /^(http[s]*[:]\/\/)([\d\.\w]+)(?:\:(\d+))?(\/.+)$/;

            var result = url.match(regex);

            if( result ) {
                return "" + result[1] + this.address + result[4];
            }
        }    
        return url;
        
    }
};

//document.addEventListener("deviceready", function() { proxy.enabled = false; }, false);


