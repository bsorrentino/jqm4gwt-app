package org.bsc.jqm.client;

public interface JQMInitializable<T> {

	void init( T initValue );
}
