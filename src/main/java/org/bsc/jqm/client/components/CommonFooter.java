package org.bsc.jqm.client.components;

import com.sksamuel.jqm4gwt.DataIcon;
import com.sksamuel.jqm4gwt.IconPos;
import com.sksamuel.jqm4gwt.Transition;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.toolbar.JQMFooter;
import com.sksamuel.jqm4gwt.toolbar.JQMNavBar;

/*
<!-- 
	<t:JQMFooter fixed="true"   >
	<t:widget>
         <t:JQMNavBar iconPos="TOP" >
         <t:button>
            <b:JQMButton	
            				ui:field="btnHome" 
            				href="#home" 
            				transition="NONE" 
            				text="Home"
   	          				builtInIcon="HOME"
   	          				
   	          				
   	          				/>
         	 </t:button>
         <t:button>
            <b:JQMButton	 
            				href="Home" 
            				transition="NONE" 
            				text="#linee"
            				iconURL="bus"
   	          				/>
         </t:button>
         <t:button>
            <b:JQMButton	
            				href="News" 
            				transition="NONE" 
            				text="News"
            				iconURL="news"
   	          				/>
         </t:button>
         <t:button>
            <b:JQMButton	
            				text=""
            				iconURL="clear"
            				html="&nbsp;"
   	          				/>
         </t:button>
         <t:button>
            <b:JQMButton	
            				text=""
            				html="&nbsp;"
            				iconURL="clear"
   	          				/>
         </t:button>
         
       </t:JQMNavBar>
     </t:widget>
	</t:JQMFooter>
 -->

 */
public class CommonFooter extends JQMFooter {

	private final JQMNavBar navbar = new JQMNavBar();
	
	public CommonFooter() {
		super();

	
		navbar.add( new JQMButton("home", "#home", Transition.NONE ) {{
			this.setBuiltInIcon( DataIcon.HOME);
		}});
		
		navbar.add( new JQMButton("linee", "#linee", Transition.NONE) {{
			
			this.setCustomIcon("bus");
		}});
		navbar.add( new JQMButton("news", "#news", Transition.NONE) {{
			this.setCustomIcon("news");
			
		}});
		
		navbar.add( new JQMButton("") {{ 
			this.setCustomIcon("clear");
			this.setHtml("&nbsp");        			
		}});
		navbar.add( new JQMButton("") {{ 
			this.setCustomIcon("clear");
			this.setHtml("&nbsp");
		}} );

		navbar.setIconPos(IconPos.TOP);
		
		this.setFixed(true);
    	this.add( navbar );
	}	
	
	
	
	public void setButtonActive( int index )  {
		if( index < 0  ) return;
		
		final JQMButton b = navbar.getButton(index);
		
		if( b != null ) {
			
			//JQMCommon.setBtnActive(b, true);
			b.getElement().addClassName("ui-state-persist");
		}
	}
	

}
