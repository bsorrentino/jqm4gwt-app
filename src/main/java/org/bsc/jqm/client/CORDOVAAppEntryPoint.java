package org.bsc.jqm.client;

import static org.bsc.jqm.client.js.JS.console;

import org.bsc.jqm.client.js.cordova.pages.CordovaHomePage;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.sksamuel.jqm4gwt.JQMContext;
import com.sksamuel.jqm4gwt.ScriptUtils;

/**
 *
 * @author softphone
 */
public class CORDOVAAppEntryPoint implements EntryPoint {

    @Override
    public void onModuleLoad() {

        ScriptUtils.waitJqmLoaded(new Callback<Void, Throwable>() {

            @Override
            public void onSuccess(Void result) {
                runApp();
            }

            @Override
            public void onFailure(Throwable reason) {
                Window.alert(reason.getMessage());
            }
        });

    }
    
    public void setTimeout() {

        final Timer t = new Timer() {
            @Override
            public void run() {
                console.log("TEST!!!!");
            }
        };
        t.scheduleRepeating(1000);

    }


    private void runApp() {
        JQMContext.disableHashListening();

        /*JQMContext.setWidgetDefaults(new WidgetDefaults() {
         @Override
         public void loaded(Widget w) {
         if (w instanceof JQMSelect) ((JQMSelect) w).setThemeIfCurrentEmpty("b");
         else if (w instanceof JQMTabs) {
         //((JQMTabs) w).setThemeIfCurrentEmpty("b");
         ((JQMTabs) w).setHeaderTheme("b");
         }
         }});*/
        
        
        /*
        String css = new StringBuilder()
                .append(JQMCommon.getImageCss(DIMAIOResources.INSTANCE.home(), JQMCommon.STYLE_UI_ICON + "homeIcon"))
                .append(JQMCommon.getImageCss(DIMAIOResources.INSTANCE.bus_x_22(), JQMCommon.STYLE_UI_ICON + "bus"))
                .append(JQMCommon.getImageCss(DIMAIOResources.INSTANCE.news(), JQMCommon.STYLE_UI_ICON + "news"))
                .append(JQMCommon.getImageCss(DIMAIOResources.INSTANCE.empty_x_22(), JQMCommon.STYLE_UI_ICON + "clear"))
                .toString();
        StyleInjector.inject(css);

        DIMAIOResources.INSTANCE.style().ensureInjected();
         */

        JQMContext.changePage(CordovaHomePage.INSTANCE.getPage());

    }

}
