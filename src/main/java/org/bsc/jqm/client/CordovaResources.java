package org.bsc.jqm.client;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface CordovaResources extends ClientBundle {

	public final CordovaResources INSTANCE = GWT.create(CordovaResources.class);

	public interface MyCss extends CssResource {
		  //@ClassName("nav-example")
	      //String navExample();
	}
	
	@Source("images/bus@22.png")
	ImageResource bus_x_22();

	@Source("images/empty@22.png")
	ImageResource empty_x_22();

	@Source("images/call.png")
	ImageResource call();

	@Source("images/callnew.png")
	ImageResource callnew();

	@Source("images/email.png")
	ImageResource email();

	@Source("images/emailnew.png")
	ImageResource emailnew();

	@Source("images/home.png")
	ImageResource home();

	@Source("images/LogoDimaio.png")
	ImageResource logoDimaio();

	@Source("images/news.png")
	ImageResource news();

	@Source("images/user.png")
	ImageResource user();

	@Source("css/dimaio.css")
	MyCss style();
}
