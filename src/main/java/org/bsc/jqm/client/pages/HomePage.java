package org.bsc.jqm.client.pages;

import org.bsc.jqm.client.js.JS;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;

public class HomePage implements JQMPageEvent.Handler {

	interface UiBinder extends
			com.google.gwt.uibinder.client.UiBinder<JQMPage, HomePage> {
	}

	public static final UiBinder BINDER = GWT.create(HomePage.UiBinder.class);

	public final static JQMPage INSTANCE = new HomePage().page;

	private final JQMPage page = HomePage.BINDER.createAndBindUi(this);

	public HomePage() {

		page.addPageHandler(this);

	}

	@UiHandler("info")
	public void info(ClickEvent e) {

		if (JS.settings.isAndroid()) {
			Window.open("credits-android/index.html", "_blank", "");
		} else {
			Window.open("credits-ios/credits.html", "_blank", "");
		}
	}

	@Override
	public void onInit(JQMPageEvent event) {
	}

	@Override
	public void onBeforeShow(JQMPageEvent event) {
		// footer.setButtonActive(0);
	}

	@Override
	public void onBeforeHide(JQMPageEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onShow(JQMPageEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onHide(JQMPageEvent event) {
		// TODO Auto-generated method stub

	}

}
