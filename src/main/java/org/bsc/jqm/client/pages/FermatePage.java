package org.bsc.jqm.client.pages;

import static org.bsc.jqm.client.js.JS.console;
import static org.bsc.jqm.client.js.JS.settings;

import org.bsc.jqm.client.JQMUtils;
import org.bsc.jqm.client.model.Fermata;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.dom.client.Element;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HasText;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.JQMPageEvent.Handler;
import com.sksamuel.jqm4gwt.Mobile;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

public class FermatePage implements Handler
{

   public static final UiBinder BINDER = GWT
         .create(FermatePage.UiBinder.class);
   public final static FermatePage INSTANCE = new FermatePage();
   private final JQMPage page = FermatePage.BINDER.createAndBindUi(this);

   private String queryString;
   private boolean reload = true;
   
   @UiField HasText header;
   @UiField JQMList listOrari;
   
   
   FermatePage()
   {
      page.addPageHandler(this);
   }

   public final JQMPage getPage()
   {
      return page;
   }

   public void init( String queryString ) {
	   this.queryString = queryString;   
	   
	   int pos = queryString.lastIndexOf('=');
	   if( pos > 0 ) {
		   header.setText( queryString.substring(++pos) );
	   }
	   reload = true;
   }
   
   private void flush(  final JsArray<Fermata> stops ) {

		for( int i = 0 ; i < stops.length() ; ++i  ) {
			
			final Fermata c = stops.get(i);
			
			listOrari.addItem( i,  new JQMListItem(c.getDescription(), "#" ) {

				{
					setAside( c.getTime() );
					addDiv( "<p>( "+ c.getPlace() + " )</p>");
					final Element a = JQMUtils.findAnchorClass(this);
					if( a!=null ) {

						a.setClassName("ui-btn");
					}
				}
			
				
			});
		/*	
			*/
			//<a href="#" class="ui-btn ui-btn-icon-right ui-icon-carat-r"><h3>Stoccarda - Rorschach - Calitri</h3></a>
		}
		
		
   }
   
   @Override
   public void onInit(JQMPageEvent event)
   {
   }

   @Override
   public void onBeforeShow(JQMPageEvent event)
   {
	    if( reload ) {
	    	listOrari.clear();
	    }
   }

   @Override
   public void onBeforeHide(JQMPageEvent event)
   {
   }

   @Override
   public void onShow(JQMPageEvent event)
   {
	    if( !reload ) return; //SKIP RELOADING
	    
		Mobile.showLoadingDialog("Loading ..");
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
				new StringBuilder().append(settings.serviceUrl())
						.append(queryString).toString());

		try {
			rb.sendRequest(null, new RequestCallback() {

				@Override
				public void onResponseReceived(Request request,
						Response response) {
					console.log("SUCCESS", response.getText());
					final JsArray<Fermata> stops = JsonUtils
							.<JsArray<Fermata>> safeEval(response.getText());

					flush( stops );
					
					Mobile.hideLoadingDialog();
					
					reload = false;
				}

				@Override
				public void onError(Request request, Throwable exception) {
					console.log("ERROR", exception);
					Mobile.hideLoadingDialog();
				}
			});
		} catch (RequestException e) {
			console.log("ERROR STARTING REQUEST", e);
			Mobile.hideLoadingDialog();
		}
   }

   @Override
   public void onHide(JQMPageEvent event)
   {
   }

   interface UiBinder extends
         com.google.gwt.uibinder.client.UiBinder<JQMPage, FermatePage>
   {
   }
}