package org.bsc.jqm.client.pages;

import static org.bsc.jqm.client.js.JS.console;

import org.bsc.jqm.client.js.Console;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

public class LineePage implements JQMPageEvent.Handler{

    interface UiBinder extends com.google.gwt.uibinder.client.UiBinder<JQMPage, LineePage> { }

    public static final UiBinder BINDER = GWT.create(LineePage.UiBinder.class);

    public final static LineePage INSTANCE = new LineePage();

    private final JQMPage page = LineePage.BINDER.createAndBindUi(this);

    @UiField JQMList listLinee;
    @UiField JQMListItem itemProvinciali;
    @UiField JQMListItem itemNazionali;
    @UiField JQMListItem itemInternazionali;
    
	public LineePage() {
		page.addPageHandler(this);
		
	}
	
	public final JQMPage getPage() {
		return page;
	}

	@UiHandler( value={"itemProvinciali","itemNazionali","itemInternazionali"} )
	public void onClick( ClickEvent event ) {
	
		final Storage storage = Storage.getSessionStorageIfSupported();
		
		if( storage==null ) {
			console.log("Session Storage doesn't supported!");
		}
	
		final JQMListItem clicked = ((JQMListItem)event.getSource());
		
		storage.setItem("linee.url", clicked.getTagStr());
		
		console.log("Session Storage is supported!", clicked.getTagStr());
	}
	
	@Override
	public void onInit(JQMPageEvent event) {

	}

	@Override
	public void onBeforeShow(JQMPageEvent event) {
	}

	@Override
	public void onBeforeHide(JQMPageEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShow(JQMPageEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onHide(JQMPageEvent event) {
		// TODO Auto-generated method stub
		
	}


}