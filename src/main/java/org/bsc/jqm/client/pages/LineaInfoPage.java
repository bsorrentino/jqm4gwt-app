package org.bsc.jqm.client.pages;

import static org.bsc.jqm.client.js.JS.console;
import static org.bsc.jqm.client.js.JS.settings;

import org.bsc.jqm.client.JQMUtils;
import org.bsc.jqm.client.model.Corsa;
import org.bsc.jqm.client.model.InfoLinea;
import org.bsc.jqm.client.model.LineaType;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HasText;
import com.sksamuel.jqm4gwt.JQMContext;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.JQMPageEvent.Handler;
import com.sksamuel.jqm4gwt.Mobile;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListDivider;
import com.sksamuel.jqm4gwt.list.JQMListItem;
import com.sksamuel.jqm4gwt.toolbar.JQMHeader;

public class LineaInfoPage implements Handler
{

   public static final UiBinder BINDER = GWT
         .create(LineaInfoPage.UiBinder.class);
   public final static LineaInfoPage INSTANCE = new LineaInfoPage();
   private final JQMPage page = LineaInfoPage.BINDER.createAndBindUi(this);

   private String queryString;
   private boolean reload = false;
   
   @UiField HasText header;
   @UiField HasText phone;
   @UiField HasText wifi;
   @UiField HasText note;
   @UiField HasText pos;
   @UiField JQMList listCorse;
   @UiField JQMHeader toolbar;
   
   LineaInfoPage()
   {
      page.addPageHandler(this);
   }

   public final JQMPage getPage() {
	return page;
   }
   
   @UiHandler("listCorse")
   public void selectCorsa( ClickEvent e ) {
	   
	   final JQMListItem item = listCorse.getClickItem();
	   
	   console.log( "selectItem", e.getSource(), String.valueOf(listCorse.getClickIndex()), item.getTagStr());
	   
	   FermatePage.INSTANCE.init(item.getTagStr());
	   
	   JQMContext.changePage(FermatePage.INSTANCE.getPage());
   }

   public void init( LineaType type, String queryString ) {
	   this.queryString = queryString;   
	   
	   int pos = queryString.lastIndexOf('=');
	   if( pos > 0 ) {
		   header.setText( queryString.substring(++pos) );
	   }
	   reload = true;
   }
   
   @Override
   public void onInit(JQMPageEvent event)
   {
	   console.log( "Service URL", settings.serviceUrl() );
   }

   @Override
   public void onBeforeShow(JQMPageEvent event)
   {
	   if( reload ) {
		   final java.util.List<JQMListDivider> dividers = listCorse.getDividers();
		   listCorse.clear();
		   if( dividers!=null ) 
			   listCorse.addDivider(0, dividers.get(0));
	   }
   }

   @Override
   public void onBeforeHide(JQMPageEvent event)
   {
   }

   private void flush( InfoLinea data) {
		wifi.setText(data.getWIFI());
		pos.setText(data.getPOS());
		phone.setText(data.getPhone());	
		note.setText(data.getNote());

		((Anchor)phone).setHref("tel:" + data.getPhone());
		
		final JsArray<Corsa> corse = data.getCorse();
		for( int i = 0 ; i < corse.length() ; ++i  ) {
			
			final Corsa c = corse.get(i);
			
			listCorse.addItem( i+1,  new JQMListItem(c.getGiorno() + " - " + c.getOra(), "#") {

				{
					setTagStr( new StringBuilder() 
						.append("GetInfoFermate.aspx?idcorsa=")
						.append(c.getId())
						.append("&corsa=")
						.append(header.getText())
						.toString()
					);
					JQMUtils.fixAnchorClass(this);
				}
			
				
			});
			//<a href="#" class="ui-btn ui-btn-icon-right ui-icon-carat-r"><h3>Stoccarda - Rorschach - Calitri</h3></a>
		}
		
		
   }
   
   @Override
   public void onShow(JQMPageEvent event)
   {
	   if( !reload ) return ;

	   listCorse.refresh();
	   
		Mobile.showLoadingDialog("Loading ..");
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
				new StringBuilder().append(settings.serviceUrl())
						.append(queryString).toString());

		try {
			rb.sendRequest(null, new RequestCallback() {

				@Override
				public void onResponseReceived(Request request,
						Response response) {
					console.log("SUCCESS", response.getText());
					final InfoLinea data = JsonUtils
							.<InfoLinea> safeEval(response.getText());

					flush( data );
					
					Mobile.hideLoadingDialog();

					reload = false;
				}

				@Override
				public void onError(Request request, Throwable exception) {
					console.log("ERROR", exception);
					Mobile.hideLoadingDialog();
				}
			});
		} catch (RequestException e) {
			console.log("ERROR STARTING REQUEST", e);
			Mobile.hideLoadingDialog();
		}
	}

   @Override
   public void onHide(JQMPageEvent event)
   {
   }

   interface UiBinder extends
         com.google.gwt.uibinder.client.UiBinder<JQMPage, LineaInfoPage>
   {
   }
}