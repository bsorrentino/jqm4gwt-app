package org.bsc.jqm.client.pages;

import static org.bsc.jqm.client.js.JS.console;
import static org.bsc.jqm.client.js.JS.settings;

import org.bsc.jqm.client.JQMUtils;
import org.bsc.jqm.client.model.New;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.uibinder.client.UiField;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.JQMPageEvent.Handler;
import com.sksamuel.jqm4gwt.Mobile;
import com.sksamuel.jqm4gwt.form.elements.JQMFilterableEvent;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

public class NewsPage implements JQMPageEvent.Handler, JQMFilterableEvent.Handler
{

   public static final UiBinder BINDER = GWT.create(NewsPage.UiBinder.class);
   public final static NewsPage INSTANCE = new NewsPage();
   private final JQMPage page = NewsPage.BINDER.createAndBindUi(this);

   @UiField JQMList listNews;
   
   NewsPage()
   {
      page.addPageHandler(this);
      listNews.addFilterableHandler(this);
   }

   public final JQMPage getPage()
   {
      return page;
   }

   @Override
   public void onInit(JQMPageEvent event)
   {
   }

   @Override
   public void onBeforeShow(JQMPageEvent event)
   {
   }

   @Override
   public void onBeforeHide(JQMPageEvent event)
   {
   }

   protected void flush( JsArray<New> data ) {
		for( int i = 0 ; i < data.length() ; ++i  ) {
			
			final New c = data.get(i);
			
			listNews.addItem( i,  new JQMListItem(c.getTitle(), "#") {

				{
					addDiv( c.getContent() );
					
					JQMUtils.fixAnchorClass(this);
				}
			
				
			});
		}
	   
   }
   
   @Override
   public void onShow(JQMPageEvent event)
   {
		Mobile.showLoadingDialog("Loading ..");
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,
				new StringBuilder().append(settings.serviceUrl())
						.append("GetActiveNews.aspx").toString());

		try {
			rb.sendRequest(null, new RequestCallback() {

				@Override
				public void onResponseReceived(Request request,
						Response response) {
					console.log("SUCCESS", response.getText());
					final JsArray<New> data = JsonUtils
							.<JsArray<New>> safeEval(response.getText());

					flush( data );
					
					Mobile.hideLoadingDialog();

				}

				@Override
				public void onError(Request request, Throwable exception) {
					console.log("ERROR", exception);
					Mobile.hideLoadingDialog();
				}
			});
		} catch (RequestException e) {
			console.log("ERROR STARTING REQUEST", e);
			Mobile.hideLoadingDialog();
		}
   }

   @Override
   public void onHide(JQMPageEvent event)
   {
   }

   interface UiBinder extends
         com.google.gwt.uibinder.client.UiBinder<JQMPage, NewsPage>
   {
   }

	@Override
	public void onBeforeFilter(JQMFilterableEvent event) {
		console.log( "onBeforeFilter", event );
	}
	
	@Override
	public Boolean onFiltering(JQMFilterableEvent event) {
		return false;
	}

}