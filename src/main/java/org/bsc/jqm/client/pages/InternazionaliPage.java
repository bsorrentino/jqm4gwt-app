package org.bsc.jqm.client.pages;

import static org.bsc.jqm.client.js.JS.console;

import org.bsc.jqm.client.model.LineaType;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.sksamuel.jqm4gwt.JQMContext;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.JQMPageEvent.Handler;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;
import com.sksamuel.jqm4gwt.toolbar.JQMHeader;

public class InternazionaliPage implements Handler
{

   public static final UiBinder BINDER = GWT
         .create(InternazionaliPage.UiBinder.class);
   public final static JQMPage INSTANCE = new InternazionaliPage().page;
   private final JQMPage page = InternazionaliPage.BINDER
         .createAndBindUi(this);

   @UiField JQMList listLinee;
   @UiField JQMHeader header;
   
   InternazionaliPage()
   {
      page.addPageHandler(this);
      
      
   }

   @UiHandler("listLinee")
   public void selectItem( ClickEvent e ) {
	   
	   final JQMListItem item = listLinee.getClickItem();
	   
	   console.log( "selectItem", e.getSource(), String.valueOf(listLinee.getClickIndex()), item.getTagStr());
	   
	   LineaInfoPage.INSTANCE.init( LineaType.INTERNAZIONALI, item.getTagStr());
	   
	   JQMContext.changePage(LineaInfoPage.INSTANCE.getPage());
   }
   
   @Override
   public void onInit(JQMPageEvent event)
   {
   }

   @Override
   public void onBeforeShow(JQMPageEvent event)
   {
   }

   @Override
   public void onBeforeHide(JQMPageEvent event)
   {
   }

   @Override
   public void onShow(JQMPageEvent event)
   {
   }

   @Override
   public void onHide(JQMPageEvent event)
   {
   }

   interface UiBinder
         extends
         com.google.gwt.uibinder.client.UiBinder<JQMPage, InternazionaliPage>
   {
   }
}