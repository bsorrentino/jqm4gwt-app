package org.bsc.jqm.client.pages;

import com.google.gwt.core.client.GWT;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;

public class ProvincialiPage implements JQMPageEvent.Handler{ 

    interface UiBinder extends com.google.gwt.uibinder.client.UiBinder<JQMPage, ProvincialiPage> { }

    public static final UiBinder BINDER = GWT.create(ProvincialiPage.UiBinder.class);

    public final static JQMPage INSTANCE = new ProvincialiPage().page;

    private final JQMPage page = ProvincialiPage.BINDER.createAndBindUi(this);

	public ProvincialiPage() {
		page.addPageHandler(this);
	}

	@Override
	public void onInit(JQMPageEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeShow(JQMPageEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBeforeHide(JQMPageEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShow(JQMPageEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onHide(JQMPageEvent event) {
		// TODO Auto-generated method stub
		
	}

}
