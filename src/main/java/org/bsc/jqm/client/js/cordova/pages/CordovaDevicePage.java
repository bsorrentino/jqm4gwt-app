package org.bsc.jqm.client.js.cordova.pages;

import org.bsc.jqm.client.js.cordova.Device;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.client.SafeHtmlTemplates.Template;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.uibinder.client.UiField;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.JQMPageEvent.Handler;
import com.sksamuel.jqm4gwt.form.JQMForm;
import com.sksamuel.jqm4gwt.form.elements.JQMCheckbox;
import com.sksamuel.jqm4gwt.form.elements.JQMText;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

public class CordovaDevicePage implements Handler {

	public static final UiBinder BINDER = GWT
			.create(CordovaDevicePage.UiBinder.class);
	public final static CordovaDevicePage INSTANCE = new CordovaDevicePage();
	private final JQMPage page = CordovaDevicePage.BINDER.createAndBindUi(this);

	public interface DeviceTemplate extends SafeHtmlTemplates {
		  @Template("<b>{0}</b>: {1}")
		  SafeHtml property( String name, Object value);

		  @Template("<span style='font-size:x-small;'>{0}</span>")
		  SafeHtml valueSmall(Object value);

	}

	static final DeviceTemplate DEVICE = GWT.create(DeviceTemplate.class);

  @UiField JQMList fields;

	CordovaDevicePage() {
		page.addPageHandler(this);

	}

        private void createForm() {
                final Device device = Device.Static.get();
                final JQMForm form = new JQMForm() {{
                    add( new JQMCheckbox("#1", "Available") {{
                        setValue(device.isAvailable());
                    }});
                    add( new JQMText("Manufacturer") {{
                        setValue( device.getManufacturer() );
                    }});
                    add( new JQMText("Model") {{
                        setValue( device.getModel());
                    }});
                    add( new JQMText("Platform") {{
                        setValue( device.getPlatform());
                    }});
                    add( new JQMText("UUID") {{
                        setValue( device.getUuid());
                    }});
                    add( new JQMText("Version") {{
                        setValue( device.getVersion());
                    }});
                }};
               page.add(form);

        }

	private void addItem( SafeHtml value ) {
		fields.appendItem( new JQMListItem().addDiv( value.asString() ) );
	}
	private void addItem( String title, SafeHtml value ) {
		fields.appendItem( new JQMListItem(title).addDiv( value.asString() ));
	}
  private String s( Object v ) { return String.valueOf(v); }

	public final JQMPage getPage() {
		return page;
	}

	@Override
	public void onInit(JQMPageEvent event) {
		final Device device = Device.Static.get();


		addItem( DEVICE.property("Available", device.isAvailable()) );
		addItem( DEVICE.property("Version",device.getVersion()) );
		addItem( DEVICE.property("Manufacturer",device.getManufacturer()) );
		addItem( DEVICE.property("Model",device.getModel()) );
		addItem( DEVICE.property("Platform", device.getPlatform()) );
		addItem("UUID:", DEVICE.valueSmall(device.getUuid()) );

	}

	@Override
	public void onBeforeShow(JQMPageEvent event) {
	}

	@Override
	public void onBeforeHide(JQMPageEvent event) {
	}

	@Override
	public void onShow(JQMPageEvent event) {
	}

	@Override
	public void onHide(JQMPageEvent event) {
	}

	interface UiBinder
			extends
				com.google.gwt.uibinder.client.UiBinder<JQMPage, CordovaDevicePage> {
	}
}
