package org.bsc.jqm.client.js;

import com.google.gwt.core.client.js.JsType;

@JsType
public interface Settings {

	String serviceUrl();
	boolean isAndroid();
	boolean isIOS();
	
    public static class Static {
    	
    	public static native Settings get()
    	/*-{
    	    return $wnd.settings;
    	}-*/;

    }

}
