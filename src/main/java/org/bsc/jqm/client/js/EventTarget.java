package org.bsc.jqm.client.js;

import com.google.gwt.core.client.js.JsType;


@JsType(prototype = "EventTarget")
public interface EventTarget {

    /**
     * 
     * @param type
     * @param listener
     * @param useCapture - capture vs bubble
     */
    <T> void addEventListener( String type, Function<T> listener, boolean useCapture);

    /**
     * 
     * @param type
     * @param listener
     * @param useCapture - capture vs bubble
     */
    <T> void removeEventListener( String type, Function<T> listener, boolean useCapture);

}
