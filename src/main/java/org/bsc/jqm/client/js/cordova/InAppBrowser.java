package org.bsc.jqm.client.js.cordova;

import org.bsc.jqm.client.js.EventTarget;
import org.bsc.jqm.client.js.Function;

import com.google.gwt.core.client.js.JsProperty;
import com.google.gwt.core.client.js.JsType;

@JsType
public interface InAppBrowser extends EventTarget {
	public String EVENT_LOADSTART = "loadstart";
	public String EVENT_LOADSTOP = "loadstop";
	public String EVENT_LOADERROR = "loaderror";
	public String EVENT_EXIT = "exit";
	
	
	public String TARGET_BLANK = "_blank";
	public String TARGET_SELF = "_self";
	public String TARGET_SYSTEM = "_system";

	void close();
    void show();
    
    void executeScript( String script, Function<Object> result );

    //insertCSS(css: { code: string }, callback: () => void): void;

    @JsType
    public interface Event {

    	/** the eventname, either loadstart, loadstop, loaderror, or exit. */
        @JsProperty
        String getType();
        
        /** the URL that was loaded. */
        @JsProperty
        String getUrl();

        /** the error code, only in the case of loaderror. */
        @JsProperty
        Integer getCode();

        /** the error message, only in the case of loaderror. */
        @JsProperty
        String getMessage();
        
    }
    
    public static class Static {

        public static native InAppBrowser open( String url, String target, String features )
        /*-{
    	    //return $wnd.open( url, target, features );
    	    return $wnd.cordova.InAppBrowser.open( url, target, features );
    	}-*/;


    }

}
