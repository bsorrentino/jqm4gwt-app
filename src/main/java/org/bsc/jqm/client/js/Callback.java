package org.bsc.jqm.client.js;

import com.google.gwt.core.client.js.JsFunction;

/**
 * 
 * @author softphone
 *
 * @param <T>
 */
@JsFunction
public interface Callback<T> {

    void f(T changed);
	
}
