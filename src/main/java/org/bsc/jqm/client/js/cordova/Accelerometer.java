package org.bsc.jqm.client.js.cordova;

import org.bsc.jqm.client.js.Callback;

import com.google.gwt.core.client.js.JsProperty;
import com.google.gwt.core.client.js.JsType;

@JsType
public interface Accelerometer {

	@JsType
	public interface Acceleration {
	    /** Amount of acceleration on the x-axis. (in m/s^2) */
	    @JsProperty
		double getX();
	    /** Amount of acceleration on the y-axis. (in m/s^2) */
	    @JsProperty
		double getY();
	    /** Amount of acceleration on the z-axis. (in m/s^2) */
	    @JsProperty
		double getZ();
	    /** Creation timestamp in milliseconds. */
	    @JsProperty
		long getTimestamp();
	}

	@JsType
	interface AccelerometerOptions {
	    /** How often to retrieve the Acceleration in milliseconds. (Default: 10000) */
	    @JsProperty
		double getFrequency();
	}
	/**
     * Stop watching the Acceleration referenced by the watchID parameter.
     * @param watchID The ID returned by navigator.accelerometer.watchAcceleration.
     */
	void clearWatch( Object watchID);

	/**
     * Get the current acceleration along the x, y, and z axes.
     * These acceleration values are returned to the accelerometerSuccess callback function.
     * @param accelerometerSuccess Success callback that gets the Acceleration object.
     * @param accelerometerError Success callback
     */
	void getCurrentAcceleration(	Callback<Acceleration> accelerometerSuccess,
									Callback<Void> accelerometerError );

		/**
     * Retrieves the device's current Acceleration at a regular interval, executing the
     * accelerometerSuccess callback function each time. Specify the interval in milliseconds
     * via the acceleratorOptions object's frequency parameter.
     * The returned watch ID references the accelerometer's watch interval, and can be used
     * with navigator.accelerometer.clearWatch to stop watching the accelerometer.
     * @param  accelerometerSuccess Callback, that called at every time interval and passes an Acceleration object.
     * @param  accelerometerError   Error callback.
     * @param  accelerometerOptions Object with options for watchAcceleration
     */
    Object watchAcceleration(	Callback<Acceleration> accelerometerSuccess,
										Callback<Void> accelerometerError,
										AccelerometerOptions accelerometerOptions);
    public static class Static {

    	public static native Accelerometer get()
    	/*-{
    	    return $wnd.navigator.accelerometer;
    	}-*/;

        public static native AccelerometerOptions options( double freq )
    	/*-{
    	    return { frequency:freq };
    	}-*/;

    }

}
