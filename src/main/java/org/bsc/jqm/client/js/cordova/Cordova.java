/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bsc.jqm.client.js.cordova;

import com.google.gwt.core.client.js.JsProperty;
import com.google.gwt.core.client.js.JsType;
import org.bsc.jqm.client.js.Function;

/**
 *
 * @author softphone
 */
@JsType
public interface Cordova {

    /**
     * Invokes native functionality by specifying corresponding service name, action and optional parameters.
     *
     * @param success A success callback function.
     * @param fail An error callback function.
     * @param service The service name to call on the native side (corresponds to a native class).
     * @param action The action name to call on the native side (generally corresponds to the native class method).
     * @param args An array of arguments to pass into the native environment.
     */
    void exec( Function success, Function fail,  String service, String action, String... args);

    /**
     * @return Gets the operating system name.
     */
    @JsProperty
    String getPlatformId();

    /**
     * @return Gets Cordova framework version
     */
    @JsProperty
    String getVersion();


    /** Defines custom logic as a Cordova module. Other modules can later access it using module name provided. */
    //define(moduleName: string, factory: (require: any, exports: any, module: any) => any): void;
    /** Access a Cordova module by name. */
    //require(moduleName: string): any;
    /** Namespace for Cordova plugin functionality */
    //plugins:CordovaPlugins;

    public static class Static {

    	public static native Cordova get()
    	/*-{
    	    return $wnd.cordova;
    	}-*/;

    }

}
