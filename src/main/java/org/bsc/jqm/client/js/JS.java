package org.bsc.jqm.client.js;

import org.bsc.jqm.client.js.cordova.Cordova;

public interface JS {

	Console console  = Console.Static.get();
	
	Settings settings = Settings.Static.get();
        
    Cordova cordova = Cordova.Static.get();
        
    Document document = Document.Static.get();
        
    /**
     * Factory for Promise creation
     */
    public static class Static {

        /**
         * Create a native wrapper function with GWT Function how parameter.
         * This is for now, when SAM will be implemented this disappear.
         *
         * @param <T>
         * @param fn
         * @return
         */
        public static native <T extends Object> Function<T> function(Function<T> fn)/*-{
         return function(e){
                console.log( 'function called', e);
                fn.f(e);  
            }
         }-*/;
    }
        
}
