/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bsc.jqm.client.js;

import com.google.gwt.core.client.js.JsType;

/**
 *
 * @author softphone
 */
@JsType(prototype = "Document")
public interface Document extends EventTarget{

    public static class Static {
        public static native Document get() /*-{
            return $doc;
        }-*/;
        
    }
    
}
