package org.bsc.jqm.client.js.cordova.pages;

import org.bsc.jqm.client.js.cordova.InAppBrowser;
import static org.bsc.jqm.client.js.JS.Static.function;
import static org.bsc.jqm.client.js.JS.console;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.JQMPageEvent.Handler;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.button.JQMButtonGroup;

public class CordovaBrowserPage implements Handler
{

   public static final UiBinder BINDER = GWT
         .create(CordovaBrowserPage.UiBinder.class);
   public final static CordovaBrowserPage INSTANCE = new CordovaBrowserPage();
   private final JQMPage page = CordovaBrowserPage.BINDER
         .createAndBindUi(this);

   @UiField JQMButtonGroup btnBrowserGroup;

   CordovaBrowserPage()
   {
      page.addPageHandler(this);
   }


   public final JQMPage getPage()
   {
      return page;
   }

   @Override
   public void onInit(JQMPageEvent event)
   {
	 for( int i = 0; i < btnBrowserGroup.getWidgetCount() ; ++i ) {
		
         final Widget w = btnBrowserGroup.getWidget(i);
         
         if (w instanceof JQMButton) {
             
        	 final JQMButton button = (JQMButton) w;
        	 
        	 button.addTapHandler(( te ) -> {
                	 
                     final InAppBrowser ref = InAppBrowser.Static.open("http://www.google.com", button.getText(), "location=yes");
                     
                     ref.addEventListener(InAppBrowser.EVENT_LOADSTART, function(( InAppBrowser.Event ev) -> { console.log("start:", ev.getUrl() ); } ), false );
                     
                     ref.addEventListener(InAppBrowser.EVENT_LOADSTOP, function((ev) -> { console.log("stop:", ev); } ), false);
                     
                     ref.addEventListener(InAppBrowser.EVENT_LOADERROR, function((ev) -> { console.log("error", ev); } ), false);
                     
                     ref.addEventListener(InAppBrowser.EVENT_EXIT, function(( InAppBrowser.Event ev) -> {  console.log( "exit",ev.getType()); } ), false);
                 }
             );
         }
		 
	 }
   }

   @Override
   public void onBeforeShow(JQMPageEvent event)
   {
   }

   @Override
   public void onBeforeHide(JQMPageEvent event)
   {
   }

   @Override
   public void onShow(JQMPageEvent event)
   {
   }

   @Override
   public void onHide(JQMPageEvent event)
   {
   }

   interface UiBinder
         extends
         com.google.gwt.uibinder.client.UiBinder<JQMPage, CordovaBrowserPage>
   {
   }
}
