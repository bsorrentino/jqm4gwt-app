/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bsc.jqm.client.js.cordova;

import com.google.gwt.core.client.js.JsProperty;
import com.google.gwt.core.client.js.JsType;

/**
 *
 * @author softphone
 */
@JsType
public interface Device {

    @JsProperty
    boolean isAvailable();

    /** Get the version of Cordova running on the device. */
    @JsProperty
    String getCordova();
    /**
     * The device.model returns the name of the device's model or product. The value is set
     * by the device manufacturer and may be different across versions of the same product.
     */
    @JsProperty
    String getModel();

    /** Get the device's operating system name. */
    @JsProperty
    String getPlatform();

    /** Get the device's Universally Unique Identifier (UUID). */
    @JsProperty
    String getUuid();

    /** Get the operating system version. */
    @JsProperty
    String getVersion();

    @JsProperty
    String getManufacturer();

    public static class Static {

    	public static native Device get()
    	/*-{
    	    return $wnd.device;
    	}-*/;

    }

}
