/**
 * 
 */
package org.bsc.jqm.client.js;

import com.google.gwt.core.client.js.JsType;

/**
 * Represent a JS Function
 * @param <P>
 */
@JsType
public interface Function<P> {

    void f(P changed);


}
