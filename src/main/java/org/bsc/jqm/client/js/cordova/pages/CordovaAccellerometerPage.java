package org.bsc.jqm.client.js.cordova.pages;

import com.sksamuel.jqm4gwt.JQMPageEvent.Handler;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.google.gwt.core.client.GWT;
import com.sksamuel.jqm4gwt.JQMPage;
import static org.bsc.jqm.client.js.JS.console;
import org.bsc.jqm.client.js.cordova.Accelerometer;
public class CordovaAccellerometerPage implements Handler {

	public static final UiBinder BINDER = GWT
			.create(CordovaAccellerometerPage.UiBinder.class);
	public final static CordovaAccellerometerPage INSTANCE = new CordovaAccellerometerPage();
	private final JQMPage page = CordovaAccellerometerPage.BINDER
			.createAndBindUi(this);

	CordovaAccellerometerPage() {
		page.addPageHandler(this);
	}

	public final JQMPage getPage() {
		return page;
	}

	@Override
	public void onInit(JQMPageEvent event) {
            
            Accelerometer a = Accelerometer.Static.get();
            
            console.log( "accelerometer", a);
            
            a.getCurrentAcceleration( 
                (p)-> {
                    console.log( "success", p);
                },  
                (p)-> {
                    console.log( "error");
                } );
            
            a.watchAcceleration(
                (p)-> {
                    console.log( "success", p);
                },  
                (p)-> {
                    console.log( "error");
                },
                Accelerometer.Static.options(1000)
                    
            );
            console.log( "getCurrentAcceleration ", a);
	}

	@Override
	public void onBeforeShow(JQMPageEvent event) {
	}

	@Override
	public void onBeforeHide(JQMPageEvent event) {
	}

	@Override
	public void onShow(JQMPageEvent event) {
	}

	@Override
	public void onHide(JQMPageEvent event) {
	}

	interface UiBinder
			extends
				com.google.gwt.uibinder.client.UiBinder<JQMPage, CordovaAccellerometerPage> {
	}
}
