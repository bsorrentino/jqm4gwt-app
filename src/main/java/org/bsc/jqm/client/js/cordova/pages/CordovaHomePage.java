package org.bsc.jqm.client.js.cordova.pages;

import static org.bsc.jqm.client.js.JS.console;
import static org.bsc.jqm.client.js.JS.cordova;
import static org.bsc.jqm.client.js.JS.document;
import static org.bsc.jqm.client.js.JS.Static.function;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiField;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.JQMPageEvent.Handler;
import com.sksamuel.jqm4gwt.button.JQMButtonGroup;
import com.sksamuel.jqm4gwt.form.elements.JQMFilterable;
import com.sksamuel.jqm4gwt.toolbar.JQMHeader;

public class CordovaHomePage implements Handler
{

   public static final UiBinder BINDER = GWT
         .create(CordovaHomePage.UiBinder.class);
   public final static CordovaHomePage INSTANCE = new CordovaHomePage();
   private final JQMPage page = CordovaHomePage.BINDER.createAndBindUi(this);

   @UiField JQMHeader header;
   @UiField JQMFilterable btnsFltr;
   @UiField JQMButtonGroup btnFeatures;
   
   final JQMPage pagesRef[] = {
	
		   CordovaDevicePage.INSTANCE.getPage(),
		   CordovaBrowserPage.INSTANCE.getPage(),
		   CordovaAccellerometerPage.INSTANCE.getPage(),
		   
   };
   
   CordovaHomePage()
   {
      page.addPageHandler(this);
   }

   public final JQMPage getPage()
   {
      return page;
   }

   @Override
   public void onInit(JQMPageEvent event)
   {
       document.addEventListener("deviceready", function((e) -> {

           console.log("cordova is ready!", "version", cordova.getVersion(), "event", e);

           //console.log("device", Device.Static.get());

           header.setText("Cordova " + cordova.getVersion());

       }), false);

   }

   @Override
   public void onBeforeShow(JQMPageEvent event)
   {
   }

   @Override
   public void onBeforeHide(JQMPageEvent event)
   {
   }

   @Override
   public void onShow(JQMPageEvent event)
   {
   }

   @Override
   public void onHide(JQMPageEvent event)
   {
   }

   interface UiBinder extends
         com.google.gwt.uibinder.client.UiBinder<JQMPage, CordovaHomePage>
   {
   }
}