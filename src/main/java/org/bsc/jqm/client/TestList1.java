package org.bsc.jqm.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiField;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.JQMPageEvent;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;
import com.google.gwt.user.client.Window;

public class TestList1 {
    interface UiBinder extends com.google.gwt.uibinder.client.UiBinder<JQMPage, TestList1> { }

    public static final UiBinder BINDER = GWT.create(TestList1.UiBinder.class);

    public static JQMPage createPage() { return new TestList1().page; }

    private final JQMPage page = TestList1.BINDER.createAndBindUi(this);


    @UiField JQMList theList;

	public TestList1() {

        final java.lang.Runnable r = () -> { Window.alert("welcome lambda!"); };

        page.addPageHandler( new JQMPageEvent.DefaultHandler() {
            @Override
            public void onShow(JQMPageEvent event) {
                //Window.alert("TestView1.page.onShow() is called");
            }
        });

        theList.addItem( 0, new JQMListItem( "Item1" ));

	}


}
