package org.bsc.jqm.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.sksamuel.jqm4gwt.list.JQMListItem;

public class JQMUtils {
	
	protected JQMUtils() {
	}

	/**
	 * 
	 * @param item
	 */
	public static void fixAnchorClass( JQMListItem item ) {
		
		final Element c = findAnchorClass(item);
		if( c != null )
			c.addClassName("ui-btn ui-btn-icon-right ui-icon-carat-r");
	}
	
	/**
	 * 
	 * @param item
	 * @return
	 */
	public static Element findAnchorClass( JQMListItem item ) {
		
		int count = DOM.getChildCount(item.getElement());
		
		for( int ii = 0 ; ii < count ; ++ii ) {
			final Element c = DOM.getChild(item.getElement(), ii);

			//console.log("tagName",c.getTagName());
			
			if( c.getTagName().equalsIgnoreCase("A")) {
				
				return c;
			}
		}

		return null;
		
	}
}
