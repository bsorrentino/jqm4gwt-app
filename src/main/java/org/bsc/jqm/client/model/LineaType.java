package org.bsc.jqm.client.model;

public enum LineaType {
	PROVINCIALI,
	NAZIONALI,
	INTERNAZIONALI
}
