package org.bsc.jqm.client.model;

import com.google.gwt.core.client.JavaScriptObject;

public class Corsa extends JavaScriptObject {

	protected Corsa() {
	}

	public final native String getId() /*-{ return this.Id; }-*/;
	public final native String getGiorno() /*-{ return this.Giorno; }-*/;
	public final native String getOra() /*-{ return this.Ora; }-*/;
	public final native String getDescrizione() /*-{ return this.Descrizione; }-*/;

}
