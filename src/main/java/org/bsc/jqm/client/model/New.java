package org.bsc.jqm.client.model;

import com.google.gwt.core.client.JavaScriptObject;

public class New extends JavaScriptObject {

	protected New() {
	}

	public final native String getTitle() /*-{ return this.Titolo; }-*/;
	public final native String getContent() /*-{ return this.CorpoTesto; }-*/;

}
