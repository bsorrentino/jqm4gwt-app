package org.bsc.jqm.client.model;

import com.google.gwt.core.client.JavaScriptObject;

public class Fermata extends JavaScriptObject {

	protected Fermata() {
	}
	
	public final native String getDescription() /*-{ return this.Fermata; }-*/;
	public final native String getPlace()		/*-{ return this.Luogo; }-*/;
	public final native String getTime()		/*-{ return this.Ora; }-*/;
	
}
