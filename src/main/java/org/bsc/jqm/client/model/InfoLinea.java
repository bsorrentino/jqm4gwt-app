package org.bsc.jqm.client.model;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class InfoLinea extends JavaScriptObject {

	protected InfoLinea() {
	}

	public final native String getPhone() /*-{ return this.telefono; }-*/;
	public final native String getWIFI() /*-{ return this.wifi; }-*/;
	public final native String getPOS() /*-{ return this.pos; }-*/;
	public final native String getNote() /*-{ return this.note; }-*/;

	public final native JsArray<Corsa> getCorse() /*-{ return this.corse; }-*/;
	
}
