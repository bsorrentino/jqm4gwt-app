# Proof Of Concept for

* GWT 2.8-SNAPSHOT (JSInterop)
* Java 8
* [JQuery Mobile for GWT 1.4.6.Final](https://github.com/jqm4gwt/jqm4gwt)
* Cordova 

### Run Super Dev Mode ###

`mvn gwt:run`